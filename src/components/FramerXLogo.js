import React from 'react';

import logo from './framerx-logo.png';

function FramerXLogo({size = 424}) {
  return <img src={logo} height={size} alt="Framer X logo" />;
}

export default FramerXLogo;