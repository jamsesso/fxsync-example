import React from 'react';

import logo from './fxsync-logo.png';

function FxSyncLogo({size = 111}) {
  return <img src={logo} height={size} alt="Framer X logo" />;
}

export default FxSyncLogo;