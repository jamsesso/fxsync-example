/** @framerx Notification */
import styled from 'styled-components';

const colors = {
  success: {
    border: 'rgba(0, 173, 38, 0.2)',
    background: 'rgba(0,173,38,0.05)'
  },
  error: {
    border: 'rgba(179, 20, 28, 0.2)',
    background: 'rgba(179, 20, 28, 0.05)'
  }
};

export default styled.div`
  background: ${props => colors[props.type].background};
  border: 1px solid ${props => colors[props.type].border};
  border-radius: 6px;
  padding: 24px 32px;
`;