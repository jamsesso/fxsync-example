import React, { Component } from 'react';
import styled from 'styled-components';

import './styles.css';
import FramerXLogo from './components/FramerXLogo';
import FxSyncLogo from './components/FxSyncLogo';

class App extends Component {
  render() {
    return (
      <Container>
        <FramerXLogo size={100} /> <span>+</span> <FxSyncLogo size={100} />
      </Container>
    );
  }
}

const Container = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;

  > span {
    font-size: 100px;
    font-weight: 100;
    margin: -20px 30px 0 30px;
  }
`;

export default App;
